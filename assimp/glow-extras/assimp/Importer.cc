#include "Importer.hh"

#include <fstream>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/profiling.hh>

#include <assimp/scene.h>
#include <assimp/Importer.hpp>

static glm::vec3 aiCast(aiVector3D const& v)
{
    return {v.x, v.y, v.z};
}
static glm::vec4 aiCast(aiColor4D const& v)
{
    return {v.r, v.g, v.b, v.a};
}

glow::assimp::Importer::Importer() {}

glow::SharedVertexArray glow::assimp::Importer::load(const std::string& filename)
{
    GLOW_ACTION();

    using namespace assimp;

    if (!std::ifstream(filename).good())
    {
        error() << "Error loading `" << filename << "' with Assimp.";
        error() << "  File not found/not readable";
        return nullptr;
    }

    uint32_t flags = aiProcess_SortByPType;

    if (mTriangulate)
        flags |= aiProcess_Triangulate;
    if (mCalculateTangents)
        flags |= aiProcess_CalcTangentSpace;
    if (mGenerateSmoothNormal)
        flags |= aiProcess_GenSmoothNormals;
    if (mGenerateUVCoords)
        flags |= aiProcess_GenUVCoords;
    if (mPreTransformVertices)
        flags |= aiProcess_PreTransformVertices;
    if (mFlipUVCoords)
        flags |= aiProcess_FlipUVs;

    return LoadAndCreateMesh(filename, flags);
}

glow::SharedVertexArray glow::assimp::Importer::LoadAndCreateMesh(std::string const & filename, uint32_t flags)
{
    MeshData data = LoadData(filename, flags);
    return CreateMeshFromData(data);
}

glow::SharedVertexArray glow::assimp::Importer::CreateMeshFromData(const MeshData & data)
{
    if (!data.valid) return nullptr;

    std::vector<SharedArrayBuffer> abs;

    if (!data.positions.empty())
    {
        auto ab = ArrayBuffer::create();
        ab->defineAttribute<glm::vec3>("aPosition");
        ab->bind().setData(data.positions);
        abs.push_back(ab);
    }
    if (!data.normals.empty())
    {
        auto ab = ArrayBuffer::create();
        ab->defineAttribute<glm::vec3>("aNormal");
        ab->bind().setData(data.normals);
        abs.push_back(ab);
    }
    if (!data.tangents.empty())
    {
        auto ab = ArrayBuffer::create();
        ab->defineAttribute<glm::vec3>("aTangent");
        ab->bind().setData(data.tangents);
        abs.push_back(ab);
    }
    for (auto i = 0u; i < data.colors.size(); ++i)
    {
        auto ab = ArrayBuffer::create();
        if (i == 0)
            ab->defineAttribute<glm::vec4>("aColor");
        else
            ab->defineAttribute<glm::vec4>("aColor" + std::to_string(i + 1));
        ab->bind().setData(data.colors[i]);
        abs.push_back(ab);
    }
    for (auto i = 0u; i < data.texCoords.size(); ++i)
    {
        auto ab = ArrayBuffer::create();
        if (i == 0)
            ab->defineAttribute<glm::vec2>("aTexCoord");
        else
            ab->defineAttribute<glm::vec2>("aTexCoord" + std::to_string(i + 1));
        ab->bind().setData(data.texCoords[i]);
        abs.push_back(ab);
    }

    for (auto const& ab : abs)
        ab->setObjectLabel(ab->getAttributes()[0].name + " of " + data.filename);

    auto eab = ElementArrayBuffer::create(data.indices);
    eab->setObjectLabel(data.filename);
    auto va = VertexArray::create(abs, eab, GL_TRIANGLES);
    va->setObjectLabel(data.filename);
    return va;
}

glow::assimp::Importer::MeshData glow::assimp::Importer::loadDataCustom(std::string const & filename)
{
    uint32_t flags = aiProcess_SortByPType;

    if (mTriangulate)
        flags |= aiProcess_Triangulate;
    if (mCalculateTangents)
        flags |= aiProcess_CalcTangentSpace;
    if (mGenerateSmoothNormal)
        flags |= aiProcess_GenSmoothNormals;
    if (mGenerateUVCoords)
        flags |= aiProcess_GenUVCoords;
    if (mPreTransformVertices)
        flags |= aiProcess_PreTransformVertices;
    if (mFlipUVCoords)
        flags |= aiProcess_FlipUVs;
    
    return LoadData(filename, flags);
}

glow::assimp::Importer::MeshData glow::assimp::Importer::LoadData(std::string const & filename, uint32_t flags)
{
    MeshData data;

    GLOW_ACTION();

    using namespace assimp;

    if (!std::ifstream(filename).good())
    {
        error() << "Error loading `" << filename << "' with Assimp.";
        error() << "  File not found/not readable";
        return data;
    }

    Assimp::Importer importer;
    auto scene = importer.ReadFile(filename, flags);

    if (!scene)
    {
        error() << "Error loading `" << filename << "' with Assimp.";
        error() << "  " << importer.GetErrorString();
        return data;
    }

    if (!scene->HasMeshes())
    {
        error() << "File `" << filename << "' has no meshes.";
        return data;
    }

    auto baseIdx = 0u;
    for (auto i = 0u; i < scene->mNumMeshes; ++i)
    {
        auto const& mesh = scene->mMeshes[i];
        auto colorsCnt = mesh->GetNumColorChannels();
        auto texCoordsCnt = mesh->GetNumUVChannels();

        if (data.texCoords.empty())
            data.texCoords.resize(texCoordsCnt);
        else if (data.texCoords.size() != texCoordsCnt)
        {
            error() << "File `" << filename << "':";
            error() << "  contains inconsistent texture coordinate counts";
            return data;
        }

        if (data.colors.empty())
            data.colors.resize(colorsCnt);
        else if (data.colors.size() != colorsCnt)
        {
            error() << "File `" << filename << "':";
            error() << "  contains inconsistent vertex color counts";
            return data;
        }

        // add faces
        auto fCnt = mesh->mNumFaces;
        for (auto f = 0u; f < fCnt; ++f)
        {
            auto const& face = mesh->mFaces[f];
            if (face.mNumIndices != 3)
            {
                error() << "File `" << filename << "':.";
                error() << "  non-3 faces not implemented/supported";
                return data;
            }
            for (auto fi = 0u; fi < face.mNumIndices; ++fi)
            {
                data.indices.push_back(baseIdx + face.mIndices[fi]);
            }
        }

        // add vertices
        auto vCnt = mesh->mNumVertices;
        for (auto v = 0u; v < vCnt; ++v)
        {
            data.positions.push_back(aiCast(mesh->mVertices[v]));

            if (mesh->HasNormals())
                data.normals.push_back(aiCast(mesh->mNormals[v]));
            if (mesh->HasTangentsAndBitangents())
                data.tangents.push_back(aiCast(mesh->mTangents[v]));

            for (auto t = 0u; t < texCoordsCnt; ++t)
                data.texCoords[t].push_back((glm::vec2)aiCast(mesh->mTextureCoords[t][v]));
            for (auto t = 0u; t < colorsCnt; ++t)
                data.colors[t].push_back(aiCast(mesh->mColors[t][v]));
        }

        baseIdx = data.positions.size();
    }

    data.filename = filename;
    data.valid = true;
    return data;
}
